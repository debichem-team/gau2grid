Author: Santiago Vila <sanvila@debian.org>
Date: Thu, 2 May 2019 22:51:02 UTC
Bug-Debian: https://bugs.debian.org/928363
Description: Fix FTBFS if build directory contains "-D"

Index: gau2grid/setup.py
===================================================================
--- gau2grid.orig/setup.py
+++ gau2grid/setup.py
@@ -84,7 +84,7 @@ if __name__ == "__main__":
     # Parse out CMake args
     setup_args = []
     for arg in sys.argv:
-        if "-D" not in arg:
+        if not arg.startswith("-D"):
             setup_args.append(arg)
             continue
 
